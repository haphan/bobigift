var BobiUI = {

    init: function(){
        BobiUI.handleTabs();
    },

    toolstipsy: function()
    {
        $('.hastip').tooltipsy({
            offset: [0,10],
            show: function (e, $el) {
                $el.fadeIn(100);
            },
            hide: function (e, $el) {
                $el.fadeOut(200);
            },
            className: 'bubbletooltip_tip'
        });
    },

    handleTabs: function (){
        $('.bobi-tabs').each(function(){
            var blockTabs = $(this);

            var handlersParent =  $('.tab-handlers', blockTabs);
            var fragmentsParent = $('.tab-fragments', blockTabs);



            $(handlersParent).children().click(function(){
                var handler = $(this);
                var index = $(handlersParent).children().index(handler);
                console.log(index);
                var fragment = $(fragmentsParent).children().eq(index).get();

                if(fragment){
                    $(handler).addClass('active');
                    $(handlersParent).children().not(handler).removeClass('active');
                    $(fragmentsParent).children().removeClass('active')
                        .eq(index).addClass('active');
                }
                return false;
            });

        });
    }

};


$(document).ready(function(){

    BobiUI.init();
    BobiUI.toolstipsy();

});
