<?php

namespace Haphan\BobiGiftBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{
    public function indexAction()
    {
        return $this->render('HaphanBobiGiftBundle:Home:index.html.twig');
    }

    public function pageAction(Request $request, $slug)
    {
        return $this->render('HaphanBobiGiftBundle:Home:page.html.twig');
    }

    public function categoryAction(Request $request, $category)
    {
        return $this->render('HaphanBobiGiftBundle:Home:list.html.twig');
    }
}
